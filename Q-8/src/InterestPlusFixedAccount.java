
public class InterestPlusFixedAccount extends FixedAccount{
public double r;



	public InterestPlusFixedAccount(int n, int b, double t, double i, double r) {
		super(n, b, t, i);
		this.r=r;
	}

	public double getYearlyIncome() {
		return (super.getYearlyIncome()+balance*r);
	}
	
	public String toString() {
		return(super.toString()+" Rate "+ r);
	}

}
