
public class main2 {

	public static void main(String[] args) {
		

		Account acc4 = new FixedAccount(1, 100, 10, 80);
		Account acc5 = new InterestAccount(2, 200, 0.10);
		Account acc6 = new InterestPlusFixedAccount(3, 300, 25, 200, 0.1);
		System.out.println(acc4);
		System.out.println(acc4.getYearlyIncome());
		System.out.println();
		System.out.println(acc5);
		System.out.println(acc5.getYearlyIncome());
		System.out.println();
		System.out.println(acc6);
		System.out.println(acc6.getYearlyIncome());

	}

}
