
public class InterestAccount extends Account {
	public double rate;
	
	
public InterestAccount(int n, int b, double r) {
		super(n, b);
		rate = r;
	}



public double getYearlyIncome() {
	
	return balance*rate;
}

public String toString() {
	return (super.toString()+" Rate "+ rate);
}

}
