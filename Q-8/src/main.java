
public class main {

	public static void main(String[] args) {
		FixedAccount acc1 = new FixedAccount(1, 100, 10, 80);
		InterestAccount acc2 = new InterestAccount(2, 200, 0.10);
		InterestPlusFixedAccount acc3 = new InterestPlusFixedAccount(3, 300, 25, 200, 0.1);

		System.out.println(acc1);
		System.out.println(acc1.getYearlyIncome());
		System.out.println();
		System.out.println(acc2);
		System.out.println(acc2.getYearlyIncome());
		System.out.println();
		System.out.println(acc3);
		System.out.println(acc3.getYearlyIncome());
		System.out.println();

	}

}
