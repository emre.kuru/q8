
public class FixedAccount extends Account {
double Threshold;
double income;

	
	
	public FixedAccount(int n, int b, double t, double i) {
		super(n, b);
	    income = i;
	    Threshold = t;
	}

	public double getYearlyIncome() {
		
		if(balance > Threshold) {
			return income;
		}
		else {
			return 0;
		}
	}

	public String toString() {
		return(super.toString()+" Threshold "+ Threshold+ " �ncome "+ income);
	}
}
