
public abstract  class Account {
int number;
int balance;

public Account(int n,  int b) {
	number=n;
	balance=b;
}

public abstract double getYearlyIncome();

public String toString() {
	return ("Account" + " Number " + number +" Balance "+ balance);
}

}
